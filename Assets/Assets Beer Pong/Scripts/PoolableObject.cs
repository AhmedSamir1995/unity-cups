﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableObject : MonoBehaviour
{
    public ObjectPool objectPool;

    public void ReturnToPool()
    {
        objectPool.ReturnToPool(gameObject);
    }


}
