﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ObjectPool:MonoBehaviour
{
    [SerializeField]
    Queue<GameObject> poolObjects;
    [SerializeField]
    GameObject originalObject;
    public int startVolume = 10;
    static Dictionary<GameObject, ObjectPool> AvailablePools;
    static ObjectPool()
    {
        AvailablePools = new Dictionary<GameObject, ObjectPool>();
    }
    public static ObjectPool Instance(GameObject originalPrefab)
    {
        if (AvailablePools.ContainsKey(originalPrefab))
        {
            return AvailablePools[originalPrefab];
        }
        else
        {
            GameObject GO = new GameObject("Pools");
            
            ObjectPool pool = GO.AddComponent<ObjectPool>();//new ObjectPool(originalPrefab);
            pool.Initialize(originalPrefab);
            return pool;
        }
    }
    public ObjectPool(GameObject prefab, int startVolume = 10 /*Action<GameObject> onGet,Action<GameObject> onReturn, Func<GameObject,GameObject> onInitialize*/)
    {
        Initialize(prefab, startVolume);
        //poolObjects = new Queue<GameObject>();
        //originalObject = prefab;
        //AvailablePools.Add(prefab, this);
        //for (int i = 0; i < startVolume; i++)
        //{
        //    InstantiateAnObject();
        //}
        //this.onGet += onGet;
        //this.onReturn += onReturn;
    }
    public void Initialize(GameObject prefab, int startVolume = 10)
    {
        poolObjects = new Queue<GameObject>();
        originalObject = prefab;
        AvailablePools.Add(prefab, this);
        this.startVolume = startVolume;
        for (int i = 0; i < startVolume; i++)
        {
            InstantiateAnObject();
        }
        //this.onGet += onGet;
        //this.onReturn += onReturn;
    }
    public event Action<GameObject> onGet;
    public event Action<GameObject> onReturn;
    public event Func<GameObject,GameObject> onInitialize;


    /// <summary>
    /// Return item to pool after usage.
    /// Alternative to destroy game object
    /// </summary>
    /// <param name="gameObject"></param>
    public void ReturnToPool(GameObject gameObject)
    {
        gameObject.SetActive(false);
        onReturn?.Invoke(gameObject);
        poolObjects.Enqueue(gameObject);
    }
    /// <summary>
    /// Get an item from pool.
    /// Alternative to Instantiate game object
    /// </summary>
    /// <param name="gameObject">Game object prefab</param>
    /// <param name="position">Set position</param>
    /// <param name="rotation">Set rotation</param>
    /// <param name="parent">Set Parent</param>
    /// <returns></returns>
    public GameObject GetObjectFromPool(Vector3 position, Quaternion rotation, Transform parent=null)
    {

        if (poolObjects.Count == 0)
        {
            InstantiateAnObject();
        }
        var item = poolObjects.Dequeue();
        item.SetActive(true);
        item.transform.position = position;
        item.transform.rotation = rotation;
        onGet?.Invoke(item);

        return item;
    }

    /// <summary>
    /// Instantiate a new Object and add it to the pool
    /// </summary>
    void InstantiateAnObject()
    {
        GameObject item = GameObject.Instantiate<GameObject>(originalObject);
        item.SetActive(false);
        var poolable = item.AddComponent<PoolableObject>();
        poolable.objectPool = this;
        if(onInitialize!=null)
            item = onInitialize(item);
        poolObjects.Enqueue(item);

    }

    private void OnDestroy()
    {
        AvailablePools.Remove(this.originalObject);
    }
}
