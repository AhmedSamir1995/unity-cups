﻿using UnityEngine;
using System.Collections;

public class Sound_pong : MonoBehaviour
{
    private GameObject Ball;
    public float VOL_Pong;
    public float Vel;
    public GameObject Sound;
    AudioSource audioSource;
    // Use this for initialization
    void OnCollisionEnter(Collision collision)
    {
      

        if (collision.relativeVelocity.sqrMagnitude > 4)
        {
            audioSource.Play();
            VOL_Pong -= 1;
            Vel += 0.1f;
            audioSource.volume -= VOL_Pong;
            if (audioSource.pitch < 2.8f)
                audioSource.pitch += Vel;

        }
    }

  
void Start ()
    {
        // Sound=GameObject.Find("Audio_pong");
        if (Sound)
            audioSource = Sound.GetComponent<AudioSource>();
        else
            audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
