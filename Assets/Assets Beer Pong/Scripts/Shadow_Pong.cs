﻿using UnityEngine;
using System.Collections;

public class Shadow_Pong : MonoBehaviour {
    public GameObject target;

    public Vector3 Offset;


    void Start()
    {
        //target = GameObject.FindGameObjectWithTag("Beer_pong");

        if (target == null || !target.activeInHierarchy)
        {
            if (GetComponent<PoolableObject>())
            {
                GetComponent<PoolableObject>().ReturnToPool();
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            Offset = transform.position - target.transform.position;
        }
    }

    void LateUpdate()
    {
        if (target == null || !target.activeInHierarchy)
        {
        }
        else
        {
            transform.position = target.transform.position + Offset;
        }   
    }
    void Update()
    {
        if (target == null || !target.activeInHierarchy)
        {
            if (GetComponent<PoolableObject>())
            {
                GetComponent<PoolableObject>().ReturnToPool();
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        if (target.tag == "Doble_Pong")
        {
            Invoke("Destroy", 1);
        }
    }

    void Destroy()
    {
        if (GetComponent<PoolableObject>())
        {
            GetComponent<PoolableObject>().ReturnToPool();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
