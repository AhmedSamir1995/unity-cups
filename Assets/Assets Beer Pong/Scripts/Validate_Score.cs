﻿using UnityEngine;
using System.Collections;

public class Validate_Score : MonoBehaviour {

    private GameObject Ball;
    public float Time_max_efect;
    public int Total_Score;
    public Transform Point_Iniciate;
    public GameObject Particles;
    public GameObject Points;
    public GameObject Bonus;
    ObjectPool particlesPool;
    ObjectPool pointsPool;

    // Use this for initialization
    void Start () {

        particlesPool = ObjectPool.Instance(Particles);
        pointsPool = ObjectPool.Instance(Points);
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Beer_pong")
        {

          Ball = other.gameObject;
          Ball.GetComponent<Rigidbody>().isKinematic = true;
            if(Ball.GetComponent<PoolableObject>())
                Ball.GetComponent<PoolableObject>().ReturnToPool();
            else
                Destroy(Ball.gameObject);
            Invoke("Score_Pong", 0);
        }
        else
        if (other.tag == "Doble_Pong")
        {
            Ball = other.gameObject;
            if (Ball.GetComponent<PoolableObject>())
                Ball.GetComponent<PoolableObject>().ReturnToPool();
            else
                Destroy(Ball.gameObject);
            Invoke("Score_Pong_Doble", 0);
        }
    }

    public void Score_Pong()
    {
        GameObject Particles_Score = particlesPool.GetObjectFromPool(Point_Iniciate.position, Camera.main.transform.rotation);
            //(GameObject)Instantiate(Particles, Point_Iniciate.position, Camera.main.transform.rotation);
        GameObject Points_Score = pointsPool.GetObjectFromPool(Point_Iniciate.position, Camera.main.transform.rotation);
            //(GameObject)Instantiate(Points,    Point_Iniciate.position, Camera.main.transform.rotation);
        Shooting_Poong.Score += Total_Score;


        if (Ball && Ball.GetComponent<PoolableObject>()) 
            Ball.GetComponent<PoolableObject>().ReturnToPool();
        else
            Destroy(Ball.gameObject);

		GetComponent<AudioSource>().Play();
        
        //Debug.Log("Cesta!");

    }
    public void Score_Pong_Doble()
    {
        Score_Pong();
        Shooting_Poong.Score += Total_Score*2;
//        Debug.Log("Cesta del bonus!");

    }

}
