﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ShooT : MonoBehaviour {

    public GameObject Manager_Pong;
    public GameObject Pointer;
    public GameObject Bar_force;
    public GameObject force;
    Slider forceSlider;
    Shooting_Poong shooting;
    // Use this for initialization
    void Start () {
        forceSlider = force.GetComponent<Slider>();
        shooting = Manager_Pong.GetComponent<Shooting_Poong>();
	}

    void OnMouseUp()
    {
        forceSlider.value = 0;
        Pointer.gameObject.SetActive(false);
        Bar_force.gameObject.SetActive(false);
        shooting.shoot();
    }
    void OnMouseDown()
    {
       
        Pointer.gameObject.SetActive(true);
        Bar_force.gameObject.SetActive(true);
    }
    // Update is called once per frame
    //void Update()
    //{

    //}
}
